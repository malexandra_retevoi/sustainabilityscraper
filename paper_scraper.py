import requests
import pandas as pd
from bs4 import BeautifulSoup

# Get Response object for webpage
page = requests.get('https://scholar.google.com/scholar?hl=en&as_sdt=0%2C5&q=CO2-Fu%C3%9Fabdruck+Lebensmittel&oq=')
# Parse webpage HTML and save as BeautifulSoup object
soup = BeautifulSoup(page.content, 'html.parser')

title_paras = soup.find_all('div', class_='gs_r gs_or gs_scl')

for para in title_paras:
    titles.append(para.find_all('span', class_='d-block')[1].find('a', class_='align-middle').text)

df = pd.DataFrame({'Title': titles, 'Abstract': abstracts})
df.to_csv('Papers.csv', index=False)